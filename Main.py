import os,sys

#---------------------------------------#
#--------   STEP 0. USER INPUTS   ------#
#---------------------------------------#

objdir    = "/home/nvillefranque/work/objects/src"
dephydir  = "/home/nvillefranque/work/dephy/dephy2ecrad/"
indir     = "/home/nvillefranque/work/data/LES/ARMCu/big_simu_film_teapot/"
ref1d     = dephydir+"inputs/i3rc_mls_cumulus.nc"
infile    = "formatted_ARMCu_1024m_332.nc"

# now that we have the file name and path:
fi = infile.split(".nc")[0]

sys.path.append(objdir)
sys.path.append(dephydir+"tools/")

from identify_clouds import idClouds
from characterize_clouds import chClouds
from make1Dprof import make1Dprof

#---------------------------------------#
#-------- STEP 1. IDENTIFY CLOUDS ------#
#---------------------------------------#

# will write object cloud field in the input file
ncfile = indir+'/'+infile
idClouds(ncfile)

#---------------------------------------#
#------ STEP 2. CHARACTERIZE CLOUDS ----#
#---------------------------------------#

# will create two charac files
ncchar2d = indir+fi+"_charac2D.nc" 
ncchar3d = indir+fi+"_charac3D.nc"
if (not os.path.exists(ncchar2d)):
  chClouds(ncfile,ncchar2d, ncchar3d)
else  : print(ncchar2d, "already exists")

#---------------------------------------#
#------   STEP 3. MAKE 1D PROFILE   ----#
#---------------------------------------#

nc1d = dephydir+"inputs/"+fi+"_1D.nc"
if (not os.path.exists(nc1d)):
  make1Dprof(ncfile,ncchar2d,nc1d,ref1d)
else  : print(nc1d, "already exists")
