import sys
import netCDF4 as nc
import matplotlib.pyplot as plt
from mycolors import basic2 as col

nc1d = "/home/villefranquen/Work/NCDF/ECRAD/i3rc_mls_cumulus.nc"
nc3d = sys.argv[1]

ds1d = nc.Dataset(nc1d,"r")
ds3d = nc.Dataset(nc3d,"r")

hl1d = ds1d.variables["height_hl"][0,:]
hl3d = ds3d.variables["height_hl"][0,:]
fl1d = 0.5*(hl1d[1:]+hl1d[:-1])
fl3d = 0.5*(hl3d[1:]+hl3d[:-1])

qv1d = ds1d.variables["q"][0,:]
qv3d = ds3d.variables["q"][0,:]

th1d = ds1d.variables["temperature_hl"][0,:]
th3d = ds3d.variables["temperature_hl"][0,:]

pr1d = ds1d.variables["pressure_hl"][0,:]
pr3d = ds3d.variables["pressure_hl"][0,:]

a=plt.subplot(131)
plt.plot(qv1d,fl1d,color=col[1],linewidth=2,marker='.',label="1D ref")
plt.plot(qv3d,fl3d,color=col[2],linewidth=2,marker='.',label="3D new")
plt.xlabel("q")
plt.legend()
plt.subplot(132,sharey=a)
plt.plot(th1d,hl1d,color=col[1],linewidth=2)
plt.plot(th3d,hl3d,color=col[2],linewidth=2)
plt.xlabel("T")
plt.subplot(133,sharey=a)
plt.plot(pr1d,hl1d,color=col[1],linewidth=2)
plt.plot(pr3d,hl3d,color=col[2],linewidth=2)
plt.xlabel("P")
plt.ylim([0,5000])
plt.show()
