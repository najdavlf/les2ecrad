import sys
from identification_methods import identify

def clouds(dictVars) :
    rc = dictVars['RCT']
    if "RIT" in dictVars: ri = dictVars['RIT']
    else: ri=0.*rc
    mask=rc*0.        # same shape as RCT
    mask[(ri+rc)>1.0e-6]=1 # cell is in cloud if liquid water mixing ratio is gt 1e-6 kg/kg
    #mask[rc>1.0e-10]=1 # thin slabs
    return mask

def idClouds(ncfile, with_ice=False):
  listVarsNames=["RCT"]
  if with_ice: listVarNames+=["RIT"]
  identify(ncfile, listVarsNames, clouds, name="clouds", overwrite=True)

if __name__ == "__main__" :
  import sys
  if len(sys.argv)<2 : print("Usage: python",sys.argv[0],"input_file"); exit(1)
  ncfile=sys.argv[1]
  dirObj = "/home/nvillefranque/work/objects/src"
  sys.path.append(dirObj)
  idClouds(ncfile)
