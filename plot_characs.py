
import matplotlib.pyplot as plt
import numpy   as np
import netCDF4 as nc
from mycolors import rainbow10 as rbw
from mycolors import basic2 as bsc
import matplotlib as mpl

nbclouds = [26,51,42,65,56,47,47,37,26,5]
cftot = [0.027222, 0.13174, 0.27071, 0.28529, 0.25912, 0.25436, 0.23169, 0.22795, 0.1254, 0.0062408]
inputs=["test_ARMCu.0%02i.nc"%i for i in range(4,14)]  

params = {"axes.titlesize":30, "axes.labelsize":28, "lines.linewidth":2, "xtick.labelsize":16,"ytick.labelsize":20,"legend.fontsize":18}
for k in params:
    mpl.rcParams[k]=params[k]

t = (np.array(range(len(cftot)))+4.)
fig,ax1=plt.subplots(figsize=(16.8,4.8))
ax1.plot(t,cftot,color=bsc[1])
ax1.set_xlabel("Time [# h]")
ax1.set_ylabel("Total cloud cover",color=bsc[1])
ax1.tick_params('y',colors=bsc[1])
ax2=ax1.twinx()
ax2.plot(t,nbclouds,color=bsc[2])
ax2.set_ylabel("Number of clouds",color=bsc[2])
ax2.tick_params('y',colors=bsc[2])
fig.tight_layout()
plt.savefig("cf_nb_ARM.png")

plt.figure(figsize=(16.8,9.6))
for i,fi in enumerate(inputs):
    dset=nc.Dataset(fi,"r")
    cf = dset.variables["cloud_fraction"][0,:][::-1]
    jmin=np.min(np.where(cf>0)[0])
    jmax=np.max(np.where(cf>0)[0])
    hl = dset.variables["height_hl"][0,:][::-1]
    fl = (hl[1:]+hl[:-1])*0.5
    ml = hl[1:-1]
    ax=plt.subplot(1,3,1)
    ovp=dset.variables["overlap_param"][0,:][::-1]
    plt.plot(ovp[jmin:jmax+1],(ml[jmin:jmax+1]-ml[jmin])/(ml[jmax]-ml[jmin]),color=rbw[i+1], label="%i th"%(i+4))
    legend=plt.legend(title="Hour",loc="best")
    plt.setp(legend.get_title(),fontsize=20)
    plt.xlabel("Overlap parameter")
    plt.ylabel("Height in cloud layer [0-1]")
    plt.subplot(1,3,2,sharey=ax)
    fsd=dset.variables["fractional_std"][0,:][::-1]
    plt.plot(fsd[jmin:jmax+1],(fl[jmin:jmax+1]-fl[jmin])/(fl[jmax]-fl[jmin]),color=rbw[i+1])
    plt.ylim(0,1)
    plt.xlabel("Fractional std")
    plt.subplot(1,3,3,sharey=ax)
    csc = .001/dset.variables["inv_cloud_effective_size"][0,:][::-1]
    plt.plot(csc[jmin:jmax+1],(fl[jmin:jmax+1]-fl[jmin])/(fl[jmax]-fl[jmin]),color=rbw[i+1])
    plt.xlabel("Cloud eff size [km]")
    plt.ylim(0,1)
plt.savefig("characs.png")
plt.show()
