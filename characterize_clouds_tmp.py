import sys
from os import walk
import numpy as np
import netCDF4 as nc
import matplotlib.pyplot as plt
import matplotlib as mpl
import itertools

dirObj = "." #/home/villefranquen/Work/codes_analyse/objects/src"
sys.path.append(dirObj)

from characterization_methods import characterize

dx=0.025
dy=0.025
dz=0.025
xx=np.linspace(0+dx/2.,6.4-dx/2.,256)
yy=np.linspace(0+dy/2.,6.4-dy/2.,256)
zz=np.linspace(0+dz/2.,4.-dz/2.,160)

def area(dictVars, indexes):
  ix = indexes[-1]
  return dx*dy*len(ix)

def demi_grand_axe(dictVars,indexes):
  iy=indexes[-2]
  ix=indexes[-1]
  xx=dictVars["W_E_direction"]
  yy=dictVars["S_N_direction"]
  if np.max(xx[ix])-np.min(xx[ix]) >= 3./4.*(np.max(xx)-np.min(xx)) :
    # on the X border
    maxgrad = np.argmax(abs(np.unique(ix)[1:]-np.unique(ix)[:-1]))
    biggestsmall = np.unique(ix)[maxgrad]
    smallestbig = np.unique(ix)[maxgrad+1]
    truexx=[xx[x]+np.max(xx)-np.min(xx) if x <= biggestsmall else xx[x] for x in ix]
  else : truexx=xx[ix]
  barycentrex = np.mean(truexx)

  if np.max(yy[iy])-np.min(yy[iy]) >= 3./4.*(np.max(xx)-np.min(yy)) :
    # on the Y border
    maxgrad = np.argmax(abs(np.unique(iy)[1:]-np.unique(iy)[:-1]))
    biggestsmall = np.unique(iy)[maxgrad]
    smallestbig = np.unique(iy)[maxgrad+1]
    trueyy=[yy[y]+np.max(yy)-np.min(yy) if y <= biggestsmall else yy[y] for y in iy]
  else : trueyy=yy[iy]
  barycentrey = np.mean(trueyy)

  dx = xx[1]-xx[0]
  dy = yy[1]-yy[0]
  maxdist = np.max(np.sqrt((truexx-barycentrex)**2 + (trueyy-barycentrey)**2))
  indmax = np.argmax(np.sqrt((truexx-barycentrex)**2 + (trueyy-barycentrey)**2)) 
  #print barycentrex,barycentrey,maxdist,truexx[indmax],trueyy[indmax],dx*dy*len(ix)
  return maxdist


def volume(dictVars,indexes):
  ncells = len(indexes[0])
  #xx = dictVars['W_E_direction']
  #yy = dictVars['S_N_direction']
  #zz = dictVars['vertical_levels']
  #dx = xx[1]-xx[0]
  #dy = yy[1]-yy[0]
  #dz = zz[1]-zz[0] # !! hypothese grilles regulieres !!
  return ncells*dx*dy*dz

def bases(dictVars,inds):
  zz = dictVars['VLEV'][:,0,0]
  dz = zz[1]-zz[0]
  # Les indices z :
  zinds = inds[-3]
  # altitude correspondant a l'indice z min du nuage
  return zz[np.min(zinds)]-dz/2.
  #uniq_cols = list(set(zip(inds[-1],inds[-2]))) # liste unique des (i,j) de chaque colonne dans le nuage
  #return dz*np.array([min([inds[-3][j] for j in np.where((np.array(inds[-1])==uniq_cols[u][0]) & (np.array(inds[-2])==uniq_cols[u][1]))[0]]) for u in range(len(uniq_cols))]) - dz/2.

def tops(dictVars,inds):
  zz = dictVars['VLEV'][:,0,0]
  dz = zz[1]-zz[0]
  # Les indices z :
  zinds = inds[-3]
  # altitude correspondant a l'indice z max du nuage
  return zz[np.max(zinds)]+dz/2.
  #uniq_cols = list(set(zip(inds[-1],inds[-2]))) # liste unique des (i,j) de chaque colonne dans le nuage
  #return dz*np.array([max([inds[-3][j] for j in np.where((np.array(inds[-1])==uniq_cols[u][0]) & (np.array(inds[-2])==uniq_cols[u][1]))[0]]) for u in range(len(uniq_cols))]) + dz/2.

def depths(dictVars,inds):
  zz = dictVars['VLEV'][:,0,0]
  dz = zz[1]-zz[0]
  uniq_cols = list(set(zip(inds[-1],inds[-2]))) # liste unique des (i,j) de chaque colonne dans le nuage
  return dz*np.array([len(np.where((np.array(inds[-1])==uniq_cols[u][0]) & (np.array(inds[-2])==uniq_cols[u][1]))[0]) for u in range(len(uniq_cols))])
  #k=zip(inds[-1],inds[-2])
  #k.sort()
  #uniq_cols = list(k for k,_ in itertools.groupby(k))
  #return [k.count(u)*dz for u in uniq_cols]


def cover(dictVars,inds):
  xx = dictVars['W_E_direction']
  yy = dictVars['S_N_direction']
  dx = xx[1]-xx[0]
  dy = yy[1]-yy[0]
  uniq_cols = list(set(zip(inds[-1],inds[-2]))) # liste unique des (i,j) de chaque colonne dans le nuage
  return len(uniq_cols)*dx*dy
  
def chClouds(ncfile,output):
  listVarNames=["W_E_direction","S_N_direction","VLEV"]#"RCT","RVT","THT","UT","VT","WT"]
  #output2D = output.split(".nc")[0]+"2D.nc"
  #characterize(ncfile, listVarNames, dictFuncCalcChar={"area km^2":area},name="clouds",dimChars={0:"time",1:"vertical_levels"}, ncdfOut=output2D)
  output3D = output.split(".nc")[0]+"3D.nc"
  #characterize(ncfile, listVarNames, dictFuncCalcChar={"tops km":tops, "bases km":bases, "depths km":depths}, name="clouds", dimChars={0:"time"}, ncdfOut=output3D)
  characterize(ncfile, listVarNames, dictFuncCalcChar={"depths km":depths}, name="clouds", dimChars={0:"time"}, ncdfOut=output3D,env=False)

if __name__ == "__main__" :
  if len(sys.argv)<2 : print("Usage: python",sys.argv[0],"input_file"); exit(1)
  ncfile=sys.argv[1]
  chClouds(ncfile,"test_cld2D_"+ncfile)
