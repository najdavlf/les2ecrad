
import os,sys,time
from os import walk
import numpy as np
import netCDF4 as nc
import matplotlib.pyplot as plt
import matplotlib as mpl

from identify_clouds import idClouds
from characterize_clouds import chClouds
from make1Dprof import make1Dprof

ref1d = "/home/villefranquen/Work/NCDF/ECRAD/i3rc_mls_cumulus.nc"
dirIn =  "/home/villefranquen/Work/NCDF/"
dirCh =  "/home/villefranquen/Work/NCDF/LEScharacs/"
dirOut = "/home/villefranquen/Work/NCDF/LES1D_ecRad/"

for (dirpath,dirnames,filesnames) in walk(dirIn):
  for file in filesnames:
    #if ".nc" in file and "od" in file :
    if "L12km.1.ARMCu.008.nc" in file :
      fi = file.split(".nc")[0]
      print fi
      ncfile = '/'.join([dirpath,file])
      idClouds(ncfile)                  # identify clouds in field 
      ncchar = dirCh+fi+"_charac.nc"
      ncchar2d = ncchar.split(".nc")[0]+"2D.nc" # contains area and demigrandaxe profiles
      if (not os.path.exists(ncchar2d)):
        chClouds(ncfile,ncchar)           # compute characteristics
      else  : print ncchar, "already exists"
      nc1d = dirOut+fi+"_1D.nc"
      if (not os.path.exists(nc1d)):
        make1Dprof(ncfile,ncchar2d,nc1d,ref1d)  # compute 1D profiles
      else  : print nc1d, "already exists"
