
import sys
import matplotlib.pyplot as plt
import numpy   as np
import netCDF4 as nc
from mycolors import rainbow15 as rbw
from mycolors import basic2 as bsc
import matplotlib as mpl

inputs=sys.argv[1:-1]
fignam=sys.argv[-1]


params = {"axes.titlesize":30, "axes.labelsize":28, "lines.linewidth":2, "xtick.labelsize":16,"ytick.labelsize":20,"legend.fontsize":18}
for k in params:
    mpl.rcParams[k]=params[k]

plt.figure(figsize=(16.8,9.6))
for i,fi in enumerate(inputs):
  lab=int(fi.split("_1D")[0].split(".")[-1])
  if lab>3:
    dset=nc.Dataset(fi,"r")
    cf = dset.variables["cloud_fraction"][0,:][::-1]
    if max(cf)<=0:
      break
    jmin=np.min(np.where(cf>0)[0])
    jmax=np.max(np.where(cf>0)[0])
    hl = dset.variables["height_hl"][0,:][::-1]
    fl = (hl[1:]+hl[:-1])*0.5
    ml = hl[1:-1]
    ax=plt.subplot(1,3,1)
    ovp=dset.variables["overlap_param"][0,:][::-1]
    plt.plot(ovp[jmin:jmax+1],(ml[jmin:jmax+1]-ml[jmin])/(ml[jmax]-ml[jmin]),color=rbw[i+1], label="%i th"%(lab))
    plt.xlim(0.3,1.)
    plt.xlabel("Overlap parameter")
    plt.ylabel("Height in cloud layer [0-1]")
    plt.subplot(1,3,2,sharey=ax)
    fsd=dset.variables["fractional_std"][0,:][::-1]
    plt.plot(fsd[jmin:jmax+1],(fl[jmin:jmax+1]-fl[jmin])/(fl[jmax]-fl[jmin]),color=rbw[i+1], label="%i th"%(lab))
    legend=plt.legend(title="Hour",loc="best")
    plt.setp(legend.get_title(),fontsize=20)
    plt.xlim(0.,1.)
    plt.ylim(0,1)
    plt.xlabel("Fractional std")
    plt.subplot(1,3,3,sharey=ax)
    csc = .001/dset.variables["inv_cloud_effective_size"][0,:][::-1]
    plt.plot(csc[jmin:jmax+1],(fl[jmin:jmax+1]-fl[jmin])/(fl[jmax]-fl[jmin]),color=rbw[i+1], label="%i th"%(lab))
    plt.xlim(0.,1.)
    plt.xlabel("Cloud eff size [km]")
    plt.ylim(0,1)
plt.savefig(fignam)
plt.show()
