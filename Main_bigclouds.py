
import sys
from os import walk
import numpy as np
import netCDF4 as nc
import matplotlib.pyplot as plt
import matplotlib as mpl

from characterize_bigclouds import chClouds
from make1Dprof import make1Dprof

dirIn =  "/home/villefranquen/Work/NCDF/LES3D"
dirCh = "/home/villefranquen/Work/NCDF/LEScharacs/"
dirOut = "/home/villefranquen/Work/NCDF/LES1D/"

for file in ["bigOnly_CERK4.1.ARMCu.%03i.nc"%i for i in range(5,13)]:
    if (".nc" in file):
      fi = file.split(".nc")[0]
      print fi
      ncfile="/".join([dirIn,"ARMCu",file])
      ncchar = dirCh+fi+"_charac.nc"
      chClouds(ncfile,ncchar)           # compute characteristics
      nc1d = dirOut+fi+"_1D.nc"
      make1Dprof(ncfile,ncchar,nc1d)    # compute 1D profiles
