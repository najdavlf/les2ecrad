
import sys
from os import walk
import numpy as np
import netCDF4 as nc
import matplotlib.pyplot as plt
import matplotlib as mpl

dirObj = "/home/villefranquen/Work/codes_analyse/scripts_objects/src"
sys.path.append(dirObj)

from characterization_methods import characterize

def area(dictVars, indexes):
  ix = indexes[0]
  xx = dictVars['W_E_direction']
  yy = dictVars['S_N_direction']
  dx = xx[1]-xx[0]
  dy = yy[1]-yy[0]
  return dx*dy*len(ix)

def chClouds(ncfile,output):
  listVarNames=["W_E_direction","S_N_direction"]
  characterize(ncfile, listVarNames, dictFuncCalcChar={"area km^2":area},name="bigclouds",dimChars={0:"time",1:"vertical_levels"}, ncdfOut=output)
