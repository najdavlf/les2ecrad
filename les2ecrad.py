#!/usr/bin/python
# -*- coding: latin-1 -*-

import sys, os 
import netCDF4 as ncdf
import numpy as np
from matplotlib import pyplot as plt
from utils import *

path = "/home/villefranquen/Work/NCDF/"
pathLES = path+"ARMCU/CERK4/"
pathECR = path+"LES1D/"

lesfile = pathLES+"CERK4.1.ARMCu.0"+num+".nc"
carfile = pathLES+"CERK4.1.ARMCu.0"+num+".obj_carac_4D.nc"
ecrfile = pathECR+"i3rc_mls_cumulus.nc"
# same as i3rc_mls_cumulus.nc but without the cloud variables
# q_liquid, inv_cloud_effective_size, cloud_fraction and fractional_std

newfile = pathECR+"i3rc_mls_cumulus_ARMCu0"+num+".nc"

os.system("cp "+ecrfile+" "+newfile)

dles = ncdf.Dataset(lesfile, 'r')
vlv_fl = dles.variables["VLEV"][:,0,0]
rct = get_var4D(dles,"RCT")
dles.close()

# vlv_fl is vertical level in LES (middle of the layer)
# hl is half level (interface between layers)
dz = vlv_fl[1]-vlv_fl[0]
vlv_hl = vlv_fl-dz/2.
vlv_hl = np.append(vlv_hl,vlv_fl[-1]+dz/2.)
cfz = cloud_fra(rct)
fstd1 = std_frac(rct)
rcz = np.mean(rct, axis=(1,2), dtype=np.float64)
dcar = ncdf.Dataset(carfile,'r')
area = dcar.variables["area"][:,0,:,0]
fstd2 = dcar.variables["RCT"][:,3,:,0]/dcar.variables["RCT"][:,0,:,0]
std = dcar.variables["RCT"][:,3,:,0]
mean = dcar.variables["RCT"][:,0,:,0]
dcar.close()
fstd2=np.nanmean(fstd2,axis=1,dtype=np.float64)
inv_cld_scale = inv_cld_scale(area)


# height is vertical level in ecRad input file
# fl is full level (middle of the layer)
decr = ncdf.Dataset(ecrfile, 'r')
height_hl = decr.variables["height_hl"][0,:]/1000.
height_hl = same_order(vlv_fl,height_hl)
height_fl = .5 * (height_hl[:-1]+height_hl[1:])
height_ml = height_hl[1:-1]
ecr_rcz = ave_to_ecrad_grid(rcz, vlv_hl, height_hl)
ecr_cfz = ave_to_ecrad_grid(cfz, vlv_hl, height_hl)
ecr_ics = ave_to_ecrad_grid(inv_cld_scale, vlv_hl, height_hl)
ecr_std = ave_to_ecrad_grid(fstd1, vlv_hl, height_hl)

cf2=cld_frac_on_ecrad_grid(rct,vlv_fl,height_hl)
rc2=rc3d_on_ecrad_grid(rct,vlv_fl,height_hl)

overlap_param=overlap_param(cf2,rc2)

dnew = ncdf.Dataset(newfile,'r+')

opvar = dnew.variables["overlap_param"]
tmpop = np.flipud(opvar[0,:])
opvar[:] = np.flipud(overlap_param)
rcvar = dnew.variables["q_liquid"]
tmprc = np.flipud(rcvar[0,:])
rcvar[:] = np.flipud(ecr_rcz)
cfvar = dnew.variables["cloud_fraction"]
tmpcf = np.flipud(cfvar[0,:])
cfvar[:] = np.flipud(cf2)
csvar = dnew.variables["inv_cloud_effective_size"]
tmpcs = np.flipud(csvar[0,:])
csvar[:] = np.flipud(ecr_ics)
sdvar = dnew.variables['fractional_std']
tmpsd = np.flipud(sdvar[0,:])
sdvar[:] = np.flipud(ecr_std)
revar = dnew.variables['re_liquid']
tmpre = np.flipud(revar[0,:])
myre = tmpre*1.
myre[cf2>0.] = 10.0e-6
revar[:] = myre

dnew.close()

if 0 :
  w,h = plt.figaspect(1)
  fig=plt.figure(figsize=(1.6*w,1.1*h))
  plt.suptitle("i3rc vs new")
  ax1=fig.add_subplot(321)
  ax1.plot(1./tmpcs,height_fl,label="i3RC")
  ax1.plot(1./ecr_ics,height_fl,'r--',label="ARMCu")
  plt.legend(loc="best")
  ax1.set_ylim(0.,4.)
  ax1.set_ylabel("Height [km")
  ax1.set_xlabel("cloud scale profile")
  ax2=fig.add_subplot(322,sharey=ax1)
  ax2.plot(tmpsd,height_fl)
  ax2.plot(ecr_std,height_fl, 'r--')
  ax2.set_ylim(0.,4.)
  ax2.set_xlabel("frac std liquid mixing ratio profile")
  ax3=fig.add_subplot(323)
  ax3.set_ylabel("Height [km")
  ax3.plot(tmprc,height_fl)
  ax3.plot(ecr_rcz,height_fl,'r--')
  ax3.set_ylim(0,4)
  ax3.set_xlabel("liquid mixing ratio profile")
  ax4=fig.add_subplot(324,sharey=ax3)
  ax4.plot(tmpcf,height_fl)
  ax4.plot(ecr_cfz,height_fl,'r--')
  ax4.set_ylim(0,4)
  ax4.set_xlabel("cloud fraction profile")
  ax5=fig.add_subplot(325,sharey=ax3)
  ax5.plot(tmpop,height_ml)
  ax5.plot(overlap_param,height_ml,'r--')
  ax5.set_ylim(0,4)
  ax5.set_xlabel("overlap parameter")


if 0 :
  w,h = plt.figaspect(1./3.)
  fig=plt.figure(figsize=(w,h))
  plt.suptitle("ARMCu08 parameters")
  ax1=fig.add_subplot(131)
  ax1.plot(1./inv_cld_scale,vlv_fl)
  #ax1.plot(1./ecr_ics,height_fl,'r--')
  ax1.set_ylim(0.,4.)
  ax1.set_ylabel("Height [km")
  ax1.set_xlabel("cloud scale profile")
  ax2=fig.add_subplot(132,sharey=ax1)
  ax2.plot(fstd1,vlv_fl)
  #ax2.plot(fstd2,vlv_fl)
  ax2.plot(ecr_std,height_fl)
  ax2.set_ylim(0.,4.)
  ax2.set_xlabel("frac std liquid mixing ratio profile")
  #ax3=fig.add_subplot(223)
  #ax3.set_ylabel("Height [km")
  #ax3.plot(rcz,vlv_fl)
  #ax3.plot(ecr_rcz,height_fl,'r--')
  #ax3.set_ylim(0,4)
  #ax3.set_xlabel("liquid mixing ratio profile")
  #ax4=fig.add_subplot(224,sharey=ax3)
  #ax4.plot(cfz,vlv_fl)
  #ax4.plot(ecr_cfz,height_fl,'r--')
  #ax4.set_ylim(0,4)
  #ax4.set_xlabel("cloud fraction profile")
  ax5=fig.add_subplot(133,sharey=ax1)
  ax5.plot(overlap_param,height_ml)
  ax5.set_ylim(0,4)
  ax5.set_xlabel("overlap parameter")
  plt.tight_layout()
  plt.savefig("ARMCu08_parameters_new.png")
plt.show()
