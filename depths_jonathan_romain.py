from netCDF4 import Dataset
from os import chdir
from numpy import *

chemin = str('D:\Projet STPE 2020\Donnees_LES')
nom = str('VMFTH.1.BOMEX.001.nc')
chdir(chemin)
nc = Dataset(nom)
bench_cloud = nc.variables['bench_cloud'][0,:,:,:]

def Depth(bench_cloud):
    nmax = ma.max(bench_cloud) # nombre de nuages identifiés (en comptant les nuages non attribués et environnement exclu)
    Depth = zeros((3,nmax)) # matrice statistique avec 3 pour 3 calculs statistique (moyenne,maximum,écart-type)

    for i in range(1,nmax+1): 
        Obj = [] # liste qui contiendra les profondeurs en nb de mailles par objet (réinitialisée à chaque objet)
        Position = where(bench_cloud==i) # Position des mailles dans le nuage (z,y,x)
        if len(Position[0])>0: # Vérification que le nuage est attribué
            X = Position[2] # coordonnées en x
            x_min = min(X)
            x_max = max(X)
            Y = Position[1] # coordonnées en y
            y_min = min(Y)
            y_max = max(Y)
            # Quadrillage de la zone où le nuage est contenu entre x_min, x_max et y_min et y_max
            for x in range(x_min,x_max+1):
                for y in range(y_min,y_max+1):
                    N = count_nonzero(bench_cloud[:,y,x]==i) # nombre de mailles appartenant au nuage identifiées sur une colonne
                    if N>0: # vérification si la colonne contient au moins une maille de nuage
                        Obj.append(N)
                        
                    
        # Calculs statistiques
        if len(Obj)>0:
            Depth[0,i-1]=mean(Obj)*0.025 # moyenne
            Depth[1,i-1]=max(Obj)*0.025 # maximum
            Depth[2,i-1]=std(Obj)*0.025 # écart-type
        else: # si nuage non attribué
            Depth[0,i-1]=nan
            Depth[1,i-1]=nan
            Depth[2,i-1]=nan
    return Depth

# je vous remets votre fonction au format characterize
# pour que vous puissiez remplacer mon depths par celui ci :
def depths(dictVars,inds):
    X = inds[2] # coordonnées en x
    x_min = min(X)
    x_max = max(X)
    Y = inds[1] # coordonnées en y
    y_min = min(Y)
    y_max = max(Y)
    # Quadrillage de la zone où le nuage est contenu entre x_min, x_max et y_min et y_max
    for x in range(x_min,x_max+1):
      for y in range(y_min,y_max+1):
        N = count_nonzero(bench_cloud[:,y,x]==i) # nombre de mailles appartenant au nuage identifiées sur une colonne
          if N>0: # vérification si la colonne contient au moins une maille de nuage
            Obj.append(N*dz)
    return Obj
