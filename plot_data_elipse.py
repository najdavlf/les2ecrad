from matplotlib.patches import Ellipse
import matplotlib.pyplot as plt
import netCDF4 as nc
import numpy as np
import sys

if len(sys.argv)<3 : print "Usage: python",sys.argv[0],"data_elipse.txt file_clouds.nc";exit(1)

clouds=np.squeeze(nc.Dataset(sys.argv[2]).variables["clouds2D"][:])
print clouds.shape
xvec = np.linspace(0,6.4,clouds.shape[1]+1)
yvec = np.linspace(0,6.4,clouds.shape[2]+1)
f = open(sys.argv[1])
for i in range(160):
  l=f.readline()
  if len(l.split())>1:
    fig,ax = plt.subplots()
    ax.pcolor(xvec,yvec,np.ma.masked_equal(clouds[i,:,:],0),cmap="Accent")
    ax.pcolor(xvec+6.4,yvec,np.ma.masked_equal(clouds[i,:,:],0),cmap="Accent")
    ax.pcolor(xvec,yvec+6.4,np.ma.masked_equal(clouds[i,:,:],0),cmap="Accent")
    ax.pcolor(xvec+6.4,yvec+6.4,np.ma.masked_equal(clouds[i,:,:],0),cmap="Accent")
    while len(l.split())>1 : 
      bx,by,md,mx,my,area = [np.float(j) for j in l.split()]
      if md>0:
        thisell = Ellipse(xy=(bx,by), width=2*md, height=2.*(area/np.pi/md), angle=np.arccos((mx-bx)/md)*180/np.pi)
        ax.add_artist(thisell)
        thisell.set_clip_box(ax.bbox)
        thisell.set_facecolor("red")
        thisell.set_alpha(.8)
      else : print "demi grand axe null !"
      l = f.readline()
    plt.savefig("clouds_level"+str(i)+".png")
  else : print "No clouds at level",l
f.close()
