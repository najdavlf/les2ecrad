from characterization_methods import characterize
import numpy as np

def area(dictVars, indexes):
  ix = indexes[-1]
  xx = dictVars['W_E_direction']
  yy = dictVars['S_N_direction']
  dx = xx[1]-xx[0]
  dy = yy[1]-yy[0]
  return dx*dy*len(ix)

def demi_grand_axe(dictVars,indexes):
  iy=indexes[-2]
  ix=indexes[-1]
  xx=dictVars["W_E_direction"]
  yy=dictVars["S_N_direction"]
  if np.max(xx[ix])-np.min(xx[ix]) >= 3./4.*(np.max(xx)-np.min(xx)) :
    # on the X border
    maxgrad = np.argmax(abs(np.unique(ix)[1:]-np.unique(ix)[:-1]))
    biggestsmall = np.unique(ix)[maxgrad]
    smallestbig = np.unique(ix)[maxgrad+1]
    truexx=[xx[x]+np.max(xx)-np.min(xx) if x <= biggestsmall else xx[x] for x in ix]
  else : truexx=xx[ix]
  barycentrex = np.mean(truexx)

  if np.max(yy[iy])-np.min(yy[iy]) >= 3./4.*(np.max(xx)-np.min(yy)) :
    # on the Y border
    maxgrad = np.argmax(abs(np.unique(iy)[1:]-np.unique(iy)[:-1]))
    biggestsmall = np.unique(iy)[maxgrad]
    smallestbig = np.unique(iy)[maxgrad+1]
    trueyy=[yy[y]+np.max(yy)-np.min(yy) if y <= biggestsmall else yy[y] for y in iy]
  else : trueyy=yy[iy]
  barycentrey = np.mean(trueyy)

  dx = xx[1]-xx[0]
  dy = yy[1]-yy[0]
  maxdist = np.max(np.sqrt((truexx-barycentrex)**2 + (trueyy-barycentrey)**2))
  indmax = np.argmax(np.sqrt((truexx-barycentrex)**2 + (trueyy-barycentrey)**2)) 
  #print barycentrex,barycentrey,maxdist,truexx[indmax],trueyy[indmax],dx*dy*len(ix)
  return maxdist

def volume(dictVars,indexes):
  ncells = len(indexes[0])
  xx = dictVars['W_E_direction']
  yy = dictVars['S_N_direction']
  zz = dictVars['vertical_levels']
  dx = xx[1]-xx[0]
  dy = yy[1]-yy[0]
  dz = zz[1]-zz[0] # !! hypothese grilles regulieres !!
  return ncells*dx*dy*dz

def bases(dictVars,inds):
  zz = dictVars['vertical_levels']
  dz = zz[1]-zz[0]
  uniq_cols = list(set(zip(inds[-1],inds[-2]))) # liste unique des (i,j) de chaque colonne dans le nuage
  # min des z pour les z de chaque colonne
  return dz*np.array([min([inds[-3][j] for j in np.where((np.array(inds[-1])==uniq_cols[u][0]) & (np.array(inds[-2])==uniq_cols[u][1]))[0]]) for u in range(len(uniq_cols))]) - dz/2.

def tops(dictVars,inds):
  zz = dictVars['vertical_levels']
  dz = zz[1]-zz[0]
  uniq_cols = list(set(zip(inds[-1],inds[-2]))) # liste unique des (i,j) de chaque colonne dans le nuage
  # max des z pour les z de chaque colonne
  return dz*np.array([max([inds[-3][j] for j in np.where((np.array(inds[-1])==uniq_cols[u][0]) & (np.array(inds[-2])==uniq_cols[u][1]))[0]]) for u in range(len(uniq_cols))]) + dz/2.

def depths(dictVars,inds):
  zz = dictVars['vertical_levels']
  dz = zz[1]-zz[0]
  uniq_cols = list(set(zip(inds[-1],inds[-2]))) # liste unique des (i,j) de chaque colonne dans le nuage
  tops = dz*np.array([max([inds[-3][j] for j in np.where((np.array(inds[-1])==uniq_cols[u][0]) & (np.array(inds[-2])==uniq_cols[u][1]))[0]]) for u in range(len(uniq_cols))]) + dz/2.
  bases = dz*np.array([min([inds[-3][j] for j in np.where((np.array(inds[-1])==uniq_cols[u][0]) & (np.array(inds[-2])==uniq_cols[u][1]))[0]]) for u in range(len(uniq_cols))]) - dz/2.
  return tops-bases

def cover(dictVars,inds):
  xx = dictVars['W_E_direction']
  yy = dictVars['S_N_direction']
  dx = xx[1]-xx[0]
  dy = yy[1]-yy[0]
  uniq_cols = list(set(zip(inds[-1],inds[-2]))) # liste unique des (i,j) de chaque colonne dans le nuage
  return len(uniq_cols)*dx*dy
  
def chClouds(ncfile,output2D, output3D, with_ice=False):
  listVarNames=["W_E_direction","S_N_direction","vertical_levels","RCT","RVT","THT"]
  if with_ice: listVarNames+=["RIT"]

  dim2D = {0:"time",1:"vertical_levels"}
  dim3D = {0:"time"}

  dic2D = {"area km^2":area, "demi_grand_axe km":demi_grand_axe}
  dic3D = {"volume km^3":volume, "bases km":bases, "tops km":tops, 
              "depths km":depths, "cover km":cover}

  for output, dic, dim in zip([output2D,output3D], [dic2D,dic3D], [dim2D, dim3D]):
    characterize(ncfile, listVarNames, name="clouds", ncdfOut=output, env=False,
            dictFuncCalcChar=dic, dimChars=dim)

if __name__ == "__main__" :
  import sys
  if len(sys.argv)<2 : print("Usage: python",sys.argv[0],"input_file"); exit(1)
  ncfile=sys.argv[1]
  dirObj = "/home/nvillefranque/work/objects/src"
  sys.path.append(dirObj)
  chClouds(ncfile,"test_cld2D_"+ncfile)
